Bobby's DNA / RNA Puzzle Pieces
===============================

Out on `jobs.local` in `/home/renderadmin/ImplicitCAD/radioshack_box`
Bobby has created a bunch of quick-printing "jigsaw" pieces that
represent **DNA** and **RNA** in that they can fit together to form
sequences:

* DNA comes in **A**, **C**, **G** and **T**
* RNA comes in **A**, **C**, **G** and **U**

There is a `rostock` directory which contains the fudge-factored 
parts for printing on the Rostock.

**NOTE**: The `.escad` files are written for Julia Longtin's fork of
[ImplicitCAD](https://github.com/colah/ImplicitCAD), not
[OpenSCAD](http://www.openscad.org/) although the two are quite
related. At the moment, I do not know where Julia's fork source code
lives.


---

